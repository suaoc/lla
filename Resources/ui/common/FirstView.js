//FirstView Component Constructor
function FirstView() {

	var TableView = require('ui/common/TableView');
	var MyWebView = require('ui/common/MyWebView');
	var NaviView = require('ui/common/NaviBtn');
	//create object instance, a parasitic subclass of Observable
	var self = Ti.UI.createView({
		backgroundColor: '#aaa',
	});
	
	var webView = new MyWebView();
	var tableView = new TableView();

	self.add(webView);
	self.add(tableView);
	if(Ti.App.smallScreen){
		var naviBtn = new NaviView(webView, tableView);
		self.add(naviBtn);
	}
	
	return self;
}

module.exports = FirstView;

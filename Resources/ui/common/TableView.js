function TableView(){
  	var ModalPicker = require('ui/common/ModalPicker');
	var osname = Ti.Platform.osname;
	var currentURL = null;
	var selfLeft = '70%';
	var selfWidth = '30%';
	var tvRows = [];
	
	if(Ti.App.smallScreen){
		selfLeft = '100%';
		selfWidth = '100%';
	}
	var self = Ti.UI.createView({
		left: selfLeft,
		width: selfWidth,
		backgroundColor: "#fff",
	});
	
	self.slideOut = function(){
		var animOut = Ti.UI.createAnimation({
		    left : '100%',
		    duration: 400,
		    curve:  Titanium.UI.ANIMATION_CURVE_EASE_IN,
		});
		self.animate(animOut);
	};	
	self.slideIn = function(){
		var animIn = Ti.UI.createAnimation({
		    left : 0,
		    duration: 400,
		    curve:  Titanium.UI.ANIMATION_CURVE_EASE_OUT,
		});
		self.animate(animIn);
	};
	
    var tHeaderHeight = '40dip';
    if(osname == 'mobileweb'){
    	tHeaderHeight = '100dip';
    }
    

    
	// languages
	var data = [{title:" English ", id: 1}, {title:" French ", id: 2}, {title:" German ", id: 3}];;
	

    var tHeader = Ti.UI.createView({
    	top: 0,
    	height: tHeaderHeight,
    });
    if(osname == 'iphone' || osname == 'ipad'){
    	var srcLng = Ti.UI.createButton({ left: 0, width: '50%', });
		var myPicker = new ModalPicker({}, data, srcLng, 0);
    	tHeader.add(srcLng);
    	var dstLng = Ti.UI.createButton({ right: 0, width: '50%', });
		var myPicker2 = new ModalPicker({}, data, dstLng, 1);
    	tHeader.add(dstLng);
	}else {
		var pickerData = [];
		data.forEach(function(v,i){
			pickerData.push(Titanium.UI.createPickerRow({title: v.title, id: v.id}));
		});

		var picker = Ti.UI.createPicker({
			top: 0,
			left: 0,
			width: '50%',
			height: tHeaderHeight,
		});
		picker.add(pickerData);
		picker.setSelectedRow(0, 0);
			
		var picker2 = Ti.UI.createPicker({
			top: 0,
			right: 0,
			width: '50%',
			height: tHeaderHeight,
		});
		picker2.add(pickerData);
		picker2.setSelectedRow(0, 1);
		
	    tHeader.add(picker);
    	tHeader.add(picker2);
	}
	self.add(tHeader);
	// var d = [ {title: 'Apples'}, {title: 'Bananas'}, {title: 'Carrots'}, {title: 'Potatoes'} ];

	var tableData = defaultTableData();
	var rowData = createTableData(tableData);
	var tableView = Ti.UI.createTableView({
		data: rowData,
		bottom: 60,
		top: tHeaderHeight,
		separatorColor: '#fff',
	});

	var addNewRowBtn = Ti.UI.createButton({
		title: 'Add Row',
		bottom: 10,
		left: 5,
	});
	
	var saveBtn = Ti.UI.createButton({
		title: 'Save',
		bottom: 10,
		right: 5,
	});

	addNewRowBtn.addEventListener('click', function(){
		console.log('clicked');
		var row = Ti.UI.createTableViewRow({});
		var tfw = Ti.UI.createTextField({
			left: 0,width: '50%',
			borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
		});

		// text field for meaning
	    var tfm = Ti.UI.createTextField({
	    	right: 0,width: '50%',
	    	borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
		});
		
	    row.add(tfw);
	    row.add(tfm);
		tableView.appendRow(row);
		tableView.scrollToIndex(tableView.data[0].rows.length - 2, null);
	});

	saveBtn.addEventListener('click', function(){
		if(!currentURL){
			console.log('invalid current url');
			alert('Failed! No URL to map.');
			return;
		}
		var allChildren = tableView.data[0].rows;
		var map = [];
		var i = 0;
		for(i=0;i < allChildren.length;i++){
			var w =  allChildren[i].getChildren()[0].getValue();
			var m = allChildren[i].getChildren()[1].getValue();
			if(m.length <=0 && w.length<=0){
				continue;
			}
			map[i] = {"word": '"' + w + '"', "meaning": '"' + m + '"' };
		}

		var srcLng, dstLng;
		if(osname=='iphone' || osname=='ipad'){
			srcLng = myPicker.title;
			dstLng = myPicker2.title;
		}else {
			srcLng = picker.getSelectedRow(0).title;
			dstLng = picker2.getSelectedRow(0).title;
		}
		console.log(srcLng);
		console.log(dstLng);
		console.log(map);
		onTableSave(currentURL, srcLng, dstLng, map);
	});
	
	self.add(tableView);
	self.add(addNewRowBtn);
	self.add(saveBtn);
	
	Ti.App.addEventListener('app:gobtn', function(e){
		currentURL = e.url;
		onLinkOpen(currentURL);
	});
	
	function defaultTableData(){
		// default empty rows
		var tableData = [];
		for(var i=0;i<Ti.App.defaultRowsNum;i++){
			tableData[i] = {word: '', meaning: ''}, {word: '', meaning: ''}, {word: '', meaning: ''};
		}
		return tableData;
	}

	function setLanguages(srcLng, dstLng){
		if(osname == 'iphone' || osname == 'ipad'){
			tHeader.getChildren()[0].title = srcLng;
			tHeader.getChildren()[1].title = dstLng;
		}else{
			
		}
	}	
	function createTableData(wordslist){
		var data = [];
		for(var i=0;i<wordslist.length;i++){
		    tvRows[i] = Ti.UI.createTableViewRow({});
		    
		    // text field for word
			var tfw = Ti.UI.createTextField({
				left: 0,
				value: wordslist[i].word,
				width: '50%',
				// backgroundColor: '#fff',
				borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
			});
	
			// text field for meaning
		    var tfm = Ti.UI.createTextField({
		    	right: 0,
				value: wordslist[i].meaning,
				width: '50%',
				// backgroundColor: '#fff',
				borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
			});
			
		    tvRows[i].add(tfw);
		    tvRows[i].add(tfm);
		    
		    data.push(tvRows[i]);
		}
		if(tvRows.length > i){
			var numToDel = tvRows.length - i;
			tvRows.splice(i, numToDel);
		}
		
		return data;
	}

	/////////////// DB SETTINGS //////////////////
	var DBSettings = {
		currentLinkID: -1,
		dbName: 'wordsdb',
		metaTblName: 'meta',
		wordsTblName: 'wordsmap',
		createTblMeta: 'CREATE TABLE IF NOT EXISTS meta (id INTEGER PRIMARY KEY, link_url TEXT, src_lng TEXT, dst_lng TEXT);',
		// INSERT INTO meta VALUES(1,'www.google.com','french','english');
		createTblWordsMap: 'CREATE TABLE IF NOT EXISTS wordsmap (id INTEGER PRIMARY KEY, link_id NUMERIC, word TEXT, meaning TEXT);',
		// INSERT INTO wordsmap VALUES(1,'applaa','apple');
	};

	function onLinkOpen(linkURL){
		console.log('onLinkOpen: url is ' + linkURL);
		var db = Ti.Database.open(DBSettings.dbName);
		try{
			// create tables 'meta' & 'wordsmap', if not exist already
			db.execute(DBSettings.createTblMeta);
			db.execute(DBSettings.createTblWordsMap);
			if(linkURL){
				// if its a valid link url
				// TODO: sqlite -- sql statement escape URL
				var linkRS = db.execute('select id, src_lng, dst_lng from meta where link_url=?;', linkURL);
				var linkID = null;
				var srcLng = null;
				var dstLng = null;
				if(linkRS.isValidRow()){
					linkID = linkRS.fieldByName('id');
					srcLng = linkRS.fieldByName('src_lng');
					dstLng = linkRS.fieldByName('dst_lng');
				}else{
					// invalid linkID -- no record for this link
				}
			}
		}catch(e){
			console.log('Failed. Reading database for ' + linkURL);
		}finally{
			linkRS.close();
		}
		
		// alert('link id: ' +  linkID);
		
		var words = [];
		if(linkID){
			var wordsRS = db.execute('select word, meaning from wordsmap where link_id=' + linkID + ';');
			while(wordsRS.isValidRow()){
				words.push({word:  wordsRS.fieldByName('word') , meaning: wordsRS.fieldByName('meaning') });
				wordsRS.next();
			}
			wordsRS.close();
			db.close();
			setLanguages(srcLng, dstLng);
			var tData = createTableData(words);
			tableView.setData(tData);
		}else{
			// alert('no link id');
			db.close();
			var dData = defaultTableData();
			var rData = createTableData(dData);
			tableView.setData(rData);
			return;			
		}
	}

	function onTableSave(linkURL, srcLng, dstLng, wordsmap){
		if(wordsmap.length <= 0){
			alert('No words list to save.');
			return;
		}
		// if record for linkURL exists, delete it from meta and wordsmap
		var db = Ti.Database.open(DBSettings.dbName);
		var metaExists = false;
		var wordsMapExists = false;
		try {
			var metaRS = db.execute("SELECT count(*) FROM sqlite_master WHERE type='table' AND name='meta';");
			if(metaRS.isValidRow()){
				var metaExists = metaRS.fieldByName('count(*)')==1;
			}
			var wordsMapRS = db.execute("SELECT count(*) FROM sqlite_master WHERE type='table' AND name='wordsmap';");
			if(wordsMapRS.isValidRow()){
				wordsMapExists = wordsMapRS.fieldByName('count(*)')==1;
			}
		}catch(e){
		}finally{
			if(metaRS) {metaRS.close();}
			if(wordsMapRS) {wordsMapRS.close();}
		}
		// we already have a record for this link -- delete that first
		var idToDel = [];
		if(metaExists){
			// delete previous entries
			try{
				var rowRS = db.execute('select id from meta where link_url=?', linkURL);
				while(rowRS.isValidRow()){
					idToDel.push(rowRS.fieldByName('id'));
					rowRS.next();
				}
			}catch(e){
				console.log('Failed. Reading IDs from meta for ' + linkURL);
			}finally{
				rowRS.close();
			}
		}
		// alert("idToDel: " + idToDel);
		try{
			db.execute('BEGIN');
			idToDel.forEach(function(v,i){
				if(v < 0){
					return;
				}
				db.execute('delete from meta where id=?', v);
				if(wordsMapExists){
					db.execute('delete from wordsmap where link_id=?;', v);
				}
			});
			// we don't have record for this link.. we might even not have the tables
			db.execute(DBSettings.createTblMeta);
			db.execute(DBSettings.createTblWordsMap);
			// 'insert into meta (link_url, src_lng, dst_lng) values ('www.google.com', 'appla','apple' );'
			db.execute('insert into meta (link_url, src_lng, dst_lng) values (?, ?, ?);', linkURL, srcLng, dstLng);
			// db.execute('insert into meta (link_url, src_lng, dst_lng) values ("http://msn.com", "German", "English");');//, linkURL, srcLng, dstLng);
			var lastLinkID = db.lastInsertRowId;
			// [{'word':'applaa', 'meaning': 'apple'}, ];
			wordsmap.forEach(function(dict, index){
				db.execute('insert into wordsmap (link_id, word, meaning) values (' + lastLinkID + ',' + dict.word + ',' + dict.meaning + ');');
				// db.execute('insert into wordsmap (link_id, word, meaning) values (1,"w","m");');
			});
			db.execute('COMMIT');
			alert('Words saved successfully!');
		}catch(e){
			console.log('Failed. Saving data for ' + linkURL);
		}finally{
			db.close();
		}		
	}
	///////////////////////////// DB SETTINGS END ///////////////////////////////////////////	
		
	return self;
}

module.exports = TableView;

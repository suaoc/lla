function MyWebView(){
	
	var osname = Ti.Platform.osname;
	var uie = require('lib/IndicatorWindow');
    
    // Create an instance of an indicator window
    var indicator = uie.createIndicatorWindow({}); 

	var selfRight = '31%';
	if(Ti.App.smallScreen){
		selfRight = 0;
	}
	
	var self = Ti.UI.createView({
		left: 0,
		right: selfRight,
		top: 0,
		bottom: 0,
	});

	var webView = Ti.UI.createWebView({
		// url: 'http://www.msn.com',
		top: 50,
		// backgroundColor: '#aaa',
		hideLoadIndicator: true,
	});
	
	var addressBarView = Ti.UI.createView({
		top: 0,
		height: 40,
		// layout: 'horizontal',
	});
	
	var prevBtn = Ti.UI.createButton({
		title: '<',
		top: 5,
		left: 0,
		height: 40,
		width: 40,
		enabled: false,
	});	
	
	var nextBtn = Ti.UI.createButton({
		title: '>',
		top: 5,
		left: 40,
		height: 40,
		width: 40,
		enabled: false,
	});	
	
	var addressField = Ti.UI.createTextField({
		top: 5,
		height: 40,
		right: 0,
		left: 80,
		borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
	});

	var goBtn = Ti.UI.createButton({
		title: 'Go',
		top: 5,
		right: 0,
		height: 40,
		width: 60,
	});	
	
	prevBtn.addEventListener('click', function(){
		webView.goBack();
	});
	
	nextBtn.addEventListener('click', function(){
		webView.goForward();
	});
	
	goBtn.addEventListener('click', function(){
		if(Ti.Platform.osname == 'android'){
	        webView.focus();
		}else{
			addressField.blur();
		}
 
		var q = addressField.getValue();
		if(!q){
			showDialog('', 'Please enter a valid URL.');
			return;
		}
		console.log(q);
		if(validURL(q) == 1){
			// url is good -- nothing to do
		}else if(validURL(q) == 2){
			// prepend http://
			q = 'http://' + q;
		}else{
			// bing search the entry and show results 
			q = 'http://www.bing.com/search?q=' + q;
		}
		// var url = 'http://www.appcelerator.com';
		webView.setUrl(q);
		// webView.reload();
		showSpinner();
		Ti.App.fireEvent('app:gobtn', {'url': q});
	});
	
	// var actInd = null;
	webView.addEventListener('beforeload', function(){
		console.log('beforeload');
		addressField.setValue(webView.url);
		showSpinner();
	});

	webView.addEventListener('error', function(e){
		// console.log(e.message);
		hideSpinner();
		alert(e.message);
	});
	
	webView.addEventListener('load', function(e) {
		addressField.setValue(e.url);
		hideSpinner();
		if(webView.canGoBack()){
			prevBtn.setEnabled(true);
		}else{
			prevBtn.setEnabled(false);
		}
		if(webView.canGoForward()){
			nextBtn.setEnabled(true);
		}else{
			nextBtn.setEnabled(false);
		}
	});
	
	Ti.App.addEventListener('linkClicked', function(params){
		console.log('link clicked: ' +  params.url);
		addressField.setValue(params.url);
		showSpinner();
	});

	function showSpinner(){
		indicator.openIndicator();
	}
	
	function hideSpinner(){
		indicator.closeIndicator();
	}

	function validURL(value) {
		var pat = /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i;
    	if(pat.test(value)){
    		return 1; // url is perfect -- already with http:// prepend
    	}else{
			var pat = /^((https?|ftp):\/\/)?(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i;
	    	if(pat.test(value)){
    			return 2; // url is valid, but needs http:// prepend
    		}
    	}
    }
    
	function showDialog(title, msg){
		var dialog = Ti.UI.createAlertDialog({
			message: msg,
			ok: 'Ok',
			title: title,
	 	}).show();
	}
	
	self.add(prevBtn);
	self.add(nextBtn);
	self.add(addressField);
	self.add(goBtn);

	self.add(webView);
	// webView.setUrl('http://www.bing.com');
	addressField.setValue('http://www.bing.com');
	return self;
}

module.exports = MyWebView;

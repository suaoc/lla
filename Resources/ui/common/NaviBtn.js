function NaviBtn(webView, tableView){
	if(!webView || !tableView){
		console.log('Navigation -- invalid views');
		return;
	}
	var navi = Ti.UI.createLabel({
		text:  "<",
		right: 0,
		height: 40,
		width: 40,
		textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
		borderRadius: 20,
		borderColor: '#fff',
		borderWidth: 2,
		color: '#fff',
		backgroundColor: '#000',
		opacity: 0.7,
	});
	navi.tableShown = false;
	navi.addEventListener('click', function(){
		if(navi.tableShown){
			tableView.slideOut();
			navi.tableShown = false;
			navi.text = "<";
			// var animLeft = Ti.UI.createAnimation({
			    // left : 0,
			    // right: undefined,
			    // duration: 750,
			// });
			// navi.animate(animLeft, function(){
				// navi.text = "    >";
				// navi.onLeft = true;
			// });
		} else {
			tableView.slideIn();
			navi.text = ">";
			navi.tableShown = true;
			// var animRight = Ti.UI.createAnimation({
			    // left: undefined,
			    // right: 0,
			    // duration: 750,
			// });
// 			
			// navi.animate(animRight, function(){
				// navi.onLeft = false;
			// });
		}
	});
	
	return navi;
}

module.exports = NaviBtn;
/**
 * Indicator window with a spinner and a label
 * 
 * @param {Object} args
 */
function createIndicatorWindow(args) {
    var width = 50,
        height = 50;

    var args = args || {};
    var top = args.top || 140;
    
    var win = Titanium.UI.createWindow({
        height:height,
        width:width,
        borderRadius:10,
        touchEnabled:false,
        backgroundColor:'#000',
        opacity:0.6
    });
    win.spinning = false;
    
    var view = Ti.UI.createView({
        width:Ti.UI.SIZE,
        height:Ti.UI.FILL,
        center: {x:(width/2), y:(height/2)},
        layout:'horizontal'
    });
    
    function osIndicatorStyle() {
        var style;
        if ('iphone' == Ti.Platform.osname || 'ipad' == Ti.Platform.osname) {
			style = Ti.UI.iPhone.ActivityIndicatorStyle.PLAIN;
        } else {
			style = Ti.UI.ActivityIndicatorStyle.DARK;	
		}
        
        return style;
    }
     
    var activityIndicator = Ti.UI.createActivityIndicator({
        style:osIndicatorStyle(),
        height:Ti.UI.FILL,
        width:30
    });
//     
    // var label = Titanium.UI.createLabel({
        // width:Ti.UI.FILL,
        // height:Ti.UI.FILL,
        // text:L('spinner'),
        // color: '#fff',
        // font: {fontFamily:'Helvetica Neue', fontSize:16, fontWeight:'bold'},
    // });

    view.add(activityIndicator);
    // view.add(label);
    win.add(view);

    function openIndicator() {
		if(win.spinning){
			return;
		}
        win.open();
        win.spinning = true;
        activityIndicator.show();
    }
    
    win.openIndicator = openIndicator;
    
    function closeIndicator() {
		if(!win.spinning){
			return;
		}
        activityIndicator.hide();
        win.close();
        win.spinning = false;
    }
    
    win.closeIndicator = closeIndicator;
    
    return win;
}

// Public interface
exports.createIndicatorWindow = createIndicatorWindow;
